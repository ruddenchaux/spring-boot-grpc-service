FROM openjdk:11-alpine
ENTRYPOINT ["/usr/bin/lol.sh"]

COPY lol.sh /usr/bin/lol.sh
COPY target/lol.jar /usr/share/lol/lol.jar
